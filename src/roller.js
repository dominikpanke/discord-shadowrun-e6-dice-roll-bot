
module.exports.rollDice = function(dice) {
  const min = dice.range.min;
  const max = dice.range.max;
  
  return Math.floor(Math.random() * Math.floor(max) + min);
}

