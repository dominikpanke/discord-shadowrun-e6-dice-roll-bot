class BotError extends Error {

};

module.exports = {
  BotError,
  codes: {
    CMD_REQUIRED: 'command-required',
    UNKNONW_CMD: 'unknown-command',
    INVALID_ARGS: 'invalid-arguments',
  }
}