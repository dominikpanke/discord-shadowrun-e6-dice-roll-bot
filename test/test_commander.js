const chai = require('chai');
const chaiAsPromised = require("chai-as-promised");
const sinon = require('sinon');

const { handleCommand } = require('../src/commander');
const { DICE_ROLL, SHADOW_ROLL } = require('../src/commands');
const { BotError, codes } = require('../src/errors');

const roller = require('../src/roller');

chai.use(chaiAsPromised);
const expect = chai.expect;

describe('Commander', function() {

  describe('handleCommand', function() {
    
    it('should ignore unknown commands', function() {
      expect(handleCommand({ })).to.be.rejectedWith(BotError, codes.UNKNONW_CM)
    });

    describe('dice-roll command', function() {

      it('should return result for rolled dice', async function() {
        const result = await handleCommand( { command: DICE_ROLL, args: ['d6', 1] } );

        expect(result.rolls[0].kind).to.equal('d6');
        expect(result.rolls[0].result).to.be.greaterThan(0);
      });

      it('should roll multiple dices of same type', async function() {
        const result = await handleCommand( { command: DICE_ROLL, args: ['d6', 4] } );

        expect(result.rolls).to.be.lengthOf(4);
      });

      it('should require kind of dice and amount of dices as arguments', async function() {
        expect(handleCommand({ command: DICE_ROLL }))
          .to.be.rejectedWith(BotError, codes.INVALID_ARGS);

        expect(handleCommand({ command: DICE_ROLL, args: ['d6'] }))
          .to.be.rejectedWith(BotError, codes.INVALID_ARGS);
      });

      it('should require a supported kind of dice', async function() {
        expect(handleCommand({ command: DICE_ROLL, args: ['w21', 1] }), 'only supported kind of dice')
          .to.be.rejectedWith(BotError, codes.INVALID_ARGS);

        expect(handleCommand({ command: DICE_ROLL, args: ['d6', 1] }), 'only supported kind of dice').to.be.ok;
      });

      it('should not accept float as amount of dice to roll', async function() {
        expect(handleCommand({ command: DICE_ROLL, args: ['d6', 1.2] }))
          .to.be.rejectedWith(BotError, codes.INVALID_ARGS);
      });
      
      it('should not accept string as amount of dice to roll', async function() {
        expect(handleCommand({ command: DICE_ROLL, args: ['d6', '1'] }))
          .to.be.rejectedWith(BotError, codes.INVALID_ARGS);
      });

      it('should require amount of dice to roll be an integer greater 0', async function() {
        expect(handleCommand({ command: DICE_ROLL, args: ['d6', 0] }))
          .to.be.rejectedWith(BotError, codes.INVALID_ARGS);
      });
    });

    describe('shadow-roll command', function() {

      afterEach(function() {
        sinon.restore();
      });

      it('should return result for rolled 6-sided dice', async function() {
        const result = await handleCommand( { command: SHADOW_ROLL, args: [1] } );

        expect(result.rolls[0].kind).to.equal('d6');
        expect(result.rolls[0].result).to.be.greaterThan(0);
      });

      it('should require amount of dice as argument', async function() {
        expect(handleCommand({ command: SHADOW_ROLL, args: [] }))
          .to.be.rejectedWith(BotError, codes.INVALID_ARGS);

        expect(handleCommand({ command: SHADOW_ROLL, args: [1] })).to.be.ok
      });

      it('should not accept float as amount of dice to roll', async function() {
        expect(handleCommand({ command: SHADOW_ROLL, args: ['d6', 1.2] }))
          .to.be.rejectedWith(BotError, codes.INVALID_ARGS);
      });
      
      it('should not accept string as amount of dice to roll', async function() {
        expect(handleCommand({ command: SHADOW_ROLL, args: ['d6', '1'] }))
          .to.be.rejectedWith(BotError, codes.INVALID_ARGS);
      });

      it('should require amount of dice to roll be an integer greater 0', async function() {
        expect(handleCommand({ command: SHADOW_ROLL, args: ['d6', 0] }))
          .to.be.rejectedWith(BotError, codes.INVALID_ARGS);
      });

      it('should return amount of successes', async function() {
        const rollDiceStub = sinon.stub(roller, 'rollDice')
          .onCall(0).returns(1)
          .onCall(1).returns(2)
          .onCall(2).returns(3)
          .onCall(3).returns(4)
          .onCall(4).returns(5)
          .onCall(5).returns(6);

        const result = await handleCommand({ command: SHADOW_ROLL, args: [6] });
        expect(result.successes).to.equal(2);
      });

      it('should return amount of dices counting as glitch', async function() {
        const rollDiceStub = sinon.stub(roller, 'rollDice')
          .onCall(0).returns(1)
          .onCall(1).returns(2)
          .onCall(2).returns(3)
          .onCall(3).returns(1)
          .onCall(4).returns(1)
          .onCall(5).returns(6);

        const result = await handleCommand({ command: SHADOW_ROLL, args: [6] });
        expect(result.glitches).to.equal(3);
      });

      it('should mark it as not beeing a glitch if glitch amount is at most half of rolled dices', async function() {
        const rollDiceStub = sinon.stub(roller, 'rollDice')
          .onCall(0).returns(1)
          .onCall(1).returns(2)
          .onCall(2).returns(3)
          .onCall(3).returns(1)
          .onCall(4).returns(1)
          .onCall(5).returns(6);

        const result = await handleCommand({ command: SHADOW_ROLL, args: [6] });
        expect(result.glitch).to.be.false;
      });

      it('should mark it as a glitch if amount of glitches is at least half of rolled dices', async function() {
        const rollDiceStub = sinon.stub(roller, 'rollDice')
          .onCall(0).returns(1)
          .onCall(1).returns(2)
          .onCall(2).returns(3)
          .onCall(3).returns(1)
          .onCall(4).returns(1);


        const result = await handleCommand({ command: SHADOW_ROLL, args: [5] });
        expect(result.glitch).to.be.true;
      });

      it('should mark it as a not critical glitch if it is a glitch but at least one success is counted', async function() {
        const rollDiceStub = sinon.stub(roller, 'rollDice')
          .onCall(0).returns(1)
          .onCall(1).returns(2)
          .onCall(2).returns(6)
          .onCall(3).returns(1)
          .onCall(4).returns(1);

        const result = await handleCommand({ command: SHADOW_ROLL, args: [5] });
        expect(result.glitch).to.be.true;
        expect(result.crit).to.be.false;
      });

      it('should mark it as a critical glitch if it is a glitch and zero succcesses are counted', async function() {
        const rollDiceStub = sinon.stub(roller, 'rollDice')
          .onCall(0).returns(1)
          .onCall(1).returns(2)
          .onCall(2).returns(4)
          .onCall(3).returns(1)
          .onCall(4).returns(1);

        const result = await handleCommand({ command: SHADOW_ROLL, args: [5] });
        expect(result.glitch).to.be.true;
        expect(result.crit).to.be.true;
      });

      it('should respect the user setting to determine what counts as a glitch role', async function() {
        const rollDiceStub = sinon.stub(roller, 'rollDice')
          .onCall(0).returns(1)
          .onCall(1).returns(2)
          .onCall(2).returns(2)
          .onCall(3).returns(5);

        const result = await handleCommand({ command: SHADOW_ROLL, args: [4] }, 'n/a', { shadowrun: { glitchRollThreshold: 2 }});
        expect(result.glitch).to.be.true;
      });

      it('should respect the user setting to determine what counts as a critical glitch role', async function() {
        const rollDiceStub = sinon.stub(roller, 'rollDice')
          .onCall(0).returns(1)
          .onCall(1).returns(2)
          .onCall(2).returns(2)
          .onCall(3).returns(4);

        const result = await handleCommand({ command: SHADOW_ROLL, args: [4] }, 'n/a', { shadowrun: { glitchRollThreshold: 2, critRollThreshold: 2, }});
        expect(result.crit).to.be.true;
      });

      it('should support critical glitch threshold to be lower than glitch threshold', async function() {
        const rollDiceStub = sinon.stub(roller, 'rollDice')
          .onCall(0).returns(2)
          .onCall(1).returns(2)
          .onCall(2).returns(3)
          .onCall(3).returns(4);

        const result = await handleCommand({ command: SHADOW_ROLL, args: [4] }, 'n/a', { shadowrun: { glitchRollThreshold: 3, critRollThreshold: 2 }});
        expect(result.glitch).to.be.true;
        expect(result.crit).to.be.false;
      });
      
      it('should support unlucky disadvantage', async function() {
        const rollDiceStub = sinon.stub(roller, 'rollDice')
          .onCall(0).returns(1)
          .onCall(1).returns(1)
          .onCall(2).returns(2)
          .onCall(3).returns(4);

        const result = await handleCommand({ command: SHADOW_ROLL, args: [4] }, 'n/a', { shadowrun: { glitchRollThreshold: 2, critRollThreshold: 1 }});
        expect(result.glitch).to.be.true;
        expect(result.crit).to.be.false;
      });

    });
    
  });

});