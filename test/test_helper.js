const expect = require('chai').expect
const { BotError, codes } = require('../src/errors');
const { isBotMessage, parseBotMessage } = require('../src/helper');
const { SHADOW_ROLL, DICE_ROLL } = require('../src/commands');

describe('Helper', function() {

  describe('isBotMessage', function() {

    it('should consider it a msg to the bot when it starts with !s', function() {
      expect(isBotMessage( { content: '!s cmd arg' } ))
        .to.be.true;
    });

  });

  describe('parseBotMessage', function() {

    it('should require a command', function() {
      // TODO: mock config - or add common default config
      expect(function() {
        parseBotMessage( { content: '!r' } )
      }).to.throw(BotError, codes.CMD_REQUIRED);
    });

    it('should return the given command as type', function() {
      expect(
        parseBotMessage( { content: '!r action' } )
      ).to.deep.include({ 
        command: 'action',
      });
    });    
    
    it('should not accept an empty command', function() {
      expect(function() {
        parseBotMessage( { content: '!r  ' } )
      }).to.throw(BotError, codes.CMD_REQUIRED);
    });    
    
    it('should return optional argument', function() {
      expect(
        parseBotMessage( { content: '!r action arg1' } )
      ).to.deep.include({ 
        command: 'action',
        args: ['arg1'],
      });
    });

    it('should return multiple optional arguments', function() {
      expect(
        parseBotMessage( { content: '!r action arg1 arg2 arg3' } )
      ).to.deep.include({ 
        command: 'action',
        args: ['arg1', 'arg2', 'arg3'],
      });
    });
    
    it('should be strict about separation of one whitespace before command', function() {
      expect(function() {
        parseBotMessage( { content: '!r  action arg1' } )
      }).to.throw(BotError, codes.CMD_REQUIRED);
    });
    
    it('should simply ignore empty arguments', function() {
      expect(
        parseBotMessage( { content: '!r action   arg1' } )
      ).to.deep.include({ 
        command: 'action',
        args: ['arg1'],
      });
    });
    
    it('should parse numeric arguments as such', function() {
      expect(
        parseBotMessage( { content: '!r action arg1 2 3.1' } )
      ).to.deep.include({ 
        command: 'action',
        args: ['arg1', 2, 3.1],
      });
    });

    describe('shorthands', function() {
    
      it('should resolve any integer as amount of dices for shadow roll', function() {
        expect(
          parseBotMessage( { content: '!r 10' } )
        ).to.deep.include({ 
          command: SHADOW_ROLL,
          args: [10],
        });
      });
    
      it('should resolve `s` followed directly by an integer as amount of dices and a shadow roll', function() {
        expect(
          parseBotMessage( { content: '!r s7' } )
        ).to.deep.include({ 
          command: SHADOW_ROLL,
          args: [7],
        });
      });
    
      it('should resolve integer directly followed by `s` as amount of dices and a shadow roll', function() {
        expect(
          parseBotMessage( { content: '!r 89s' } )
        ).to.deep.include({ 
          command: SHADOW_ROLL,
          args: [89],
        });
      });
    
      it('should resolve `s` as a shadow roll', function() {
        expect(
          parseBotMessage( { content: '!r s 10' } )
        ).to.deep.include({ 
          command: SHADOW_ROLL,
          args: [10],
        });
      });
    
      it('should resolve `d6` as a 6-sided dice roll', function() {
        expect(
          parseBotMessage( { content: '!r d6 4' } )
        ).to.deep.include({ 
          command: DICE_ROLL,
          args: ['d6', 4],
        });
      });
    
      it('should resolve integer directly followed by `d6` as amount of amount of dices and 6-sided dice roll', function() {
        expect(
          parseBotMessage( { content: '!r 4d6' } )
        ).to.deep.include({ 
          command: DICE_ROLL,
          args: ['d6', 4],
        });
      });

    });
    
  });
  
});