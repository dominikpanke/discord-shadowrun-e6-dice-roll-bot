
module.exports = {
  getDice(kind) {
    switch(kind) {
      case 'd6': return module.exports.d6;
      default: return undefined;
    }
  },
  d6: {
    kind: 'd6',
    range: {
      min: 1,
      max: 6,
    },
    shadowrun: {
      success: [5, 6],
      failure: [1],
    },
  }
};
