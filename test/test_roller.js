const expect = require('chai').expect

const { rollDice } = require('../src/roller');

describe('Roller', function() {

  describe('rollDice', function() {

    it('should return the result of the rolled dice', function() {
      const dice = {
        range: { min: 1, max: 6 }
      };

      const res = rollDice(dice);
      expect(Number.isInteger(res), 'roll result is integer').to.be.true;
    });

    it('should roll within the min and max attributes of the given dice', function() {
      const dice = {
        range: {
          min: 1,
          max: 3
        }
      }

      const sampleSize = 100;

      let min = Number.MAX_SAFE_INTEGER;
      let max = Number.MIN_SAFE_INTEGER;
      let sum = 0.0;

      for (let i = 0; i < sampleSize; i++) {
        result = rollDice(dice)

        if (result < min) {
          min = result;
        } else if (result > max) {
          max = result;
        }

        sum += result;
      }
        
      const avg = sum / sampleSize
      expect(avg, 'avg. roll greater than min').to.be.above(dice.range.min);
      expect(avg, 'avg. roll lower than max').to.be.below(dice.range.max);
      
      expect(min, 'min roll equal to min of range').to.equal(dice.range.min);
      expect(max, 'max roll equal to max of range').to.equal(dice.range.max);
    });

  });

});