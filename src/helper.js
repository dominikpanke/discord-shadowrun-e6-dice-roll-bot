const config = require('./config');
const { BotError, codes } = require('./errors');
const { SHADOW_ROLL, DICE_ROLL } = require('./commands');

module.exports = {
  isBotMessage: (message) => {
    if (message.content.startsWith(config.get('bot.prefix'))) {
      // prepare for more checks
      return true;
    }
    return false;
  },
  
  parseBotMessage: (message) => { 
    const parts = message.content.split(' ');

    if(parts.length < 2) {
      throw new BotError(codes.CMD_REQUIRED);
    }

    const command = parts[1];
    if(command.trim().length <= 0) {
      throw new BotError(codes.CMD_REQUIRED);
    }

    const args = [];
    if(parts.length > 2) {
      module.exports._parseArguments(parts.splice(2, parts.length), args);
    }

    return module.exports._resolveShorthand(command, args)
  },

  _resolveShorthand: (command, args) => {
    if (/^\d+$/.test(command)) {
      return {
        command: SHADOW_ROLL,
        args: [parseInt(command)].concat(args)
      }
    } else if (/^\d+s$/.test(command)) {
      return {
        command: SHADOW_ROLL,
        args: [parseInt(command.replace('s', ''))].concat(args)
      }
    } else if (/^s\d+$/.test(command)) {
      return {
        command: SHADOW_ROLL,
        args: [parseInt(command.replace('s', ''))].concat(args)
      }
    } 
    else if (/^s$/.test(command)) {
      return {
        command: SHADOW_ROLL,
        args,
      }
    } else if (/^d6$/.test(command)) {
      return {
        command: DICE_ROLL,
        args: ['d6'].concat(args),
      }
    } else if (/^\d+d6$/.test(command)) {
      return {
        command: DICE_ROLL,
        args: ['d6', parseInt(command.replace('d6', ''))].concat(args),
      }
    }

    return { command, args };
  },

  _parseArguments: (source, targetArr) => {
    for(let i = 0; i < source.length; i++) {
      let part = source[i];

      if(part.length > 0) { 
        targetArr.push(module.exports._parseArgument(part));
      }
    }
  },

  _parseArgument: (part) => {
    if (isNaN(part)) {
      return part;
    } else {
      if (/^\d+$/.test(part)) {
        return parseInt(part);
      } else {
        return parseFloat(part)
      }
    }
  },
}
