const expect = require('chai').expect

const { transformError, transformBotMessage } = require('../src/transform');
const { BotError } = require('../src/errors');

const diceRoll = {
  command: 'dice-roll',
  total: 12,
  rolls: [{
      kind: 'd6',
      result: 5
    }, {
      kind: 'd6',
      result: 3
    }, {
      kind: 'd6',
      result: 3
    }, {
      kind: 'd6',
      result: 1
  }]
};

const shadowRollSuccess = {
  command: 'shadow-roll',
  total: 34,
  rolls: [{
      kind: 'd6',
      result: 5
    }, {
      kind: 'd6',
      result: 3
    }, {
      kind: 'd6',
      result: 5
    }, {
      kind: 'd6',
      result: 4
    }, {
      kind: 'd6',
      result: 3
    }, {
      kind: 'd6',
      result: 6
    }, {
      kind: 'd6',
      result: 6
    }, {
      kind: 'd6',
      result: 1
    }, {
      kind: 'd6',
      result: 1
  }],
  successes: 2,
  glitches: 2,
  glitch: false,
  crit: false,
};

const shadowRollGlitch = {
  command: 'shadow-roll',
  total: 8,
  rolls: [{
      kind: 'd6',
      result: 1
    }, {
      kind: 'd6',
      result: 5
    }, {
      kind: 'd6',
      result: 1
    }, {
      kind: 'd6',
      result: 1
  }],
  successes: 1,
  glitches: 3,
  glitch: true,
  crit: false,
};

const shadowRollCrit = {
  command: 'shadow-roll',
  total: 5,
  rolls: [{
      kind: 'd6',
      result: 1
    }, {
      kind: 'd6',
      result: 1
    }, {
      kind: 'd6',
      result: 3
  }],
  successes: 0,
  glitches: 2,
  glitch: true,
  crit: true,
};


describe('Transform', function() {

  describe('transformError', function() {

    it('should set error code as message content', function() {
      expect(transformError(new BotError('my-code')).content).to.equal('my-code');
    });

  });

  describe('transformBotMessage', function() {

    describe('dice-roll', function() {

      it('should set sum as title', function() {
        expect(transformBotMessage(diceRoll).title).to.equal('12');
      });

      it('should set equation as description', function() {
        expect(transformBotMessage(diceRoll).description).to.equal('5 + 3 + 3 + 1 = 12');
      });

    });

    describe('shadow-roll', function() {

      it('should list successes in title', function() {
        expect(transformBotMessage(shadowRollSuccess).title).to.equal('2 Erfolge');
      });

      it('should hint at glitch in title and list successes', function() {
        expect(transformBotMessage(shadowRollGlitch).title).to.equal('Patzer! - 1 Erfolg');
      });

      it('should hint at critical glitch in title', function() {
        expect(transformBotMessage(shadowRollCrit).title).to.equal('Kritischer Patzer!');
      });

      it('should list', function() {
        expect(transformBotMessage(shadowRollCrit).title).to.equal('Kritischer Patzer!');
      });

      it('should set equation as description', function() {
        expect(transformBotMessage({
          command: 'shadow-roll',
          total: 7,
          rolls: [{
              kind: 'd6',
              result: 4
            }, {
              kind: 'd6',
              result: 3
          }],
          successes: 0,
          glitches: 0,
          glitch: false,
          crit: false,
        }).description).to.equal('4 + 3 = 7');
      });

      it('should have bold 5, 6 and italic 1 in equation of description', function() {
        expect(transformBotMessage(shadowRollSuccess).description).to.equal('**5** + 3 + **5** + 4 + 3 + **6** + **6** + *1* + *1* = 34');
      });

      it('should not format the total', function() {
        expect(transformBotMessage({
          command: 'shadow-roll',
          total: 11,
          rolls: [{
              kind: 'd6',
              result: 6
            }, {
              kind: 'd6',
              result: 1
            }, {
              kind: 'd6',
              result: 4
          }],
          successes: 0,
          glitches: 0,
          glitch: false,
          crit: false,
        }).description).to.equal('**6** + *1* + 4 = 11');
      });

    });

  });

});