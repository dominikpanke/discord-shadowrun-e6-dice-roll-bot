require('dotenv').config();
const convict = require('convict');
const os = require('os');

const config = convict({
  env: {
    doc: 'The application environment.',
    format: ['prod', 'dev'],
    default: 'dev',
    env: 'NODE_ENV',
  }, 
  logLevel: {
    doc: 'log level',
    format: ['debug', 'info', 'warn', 'error'],
    default: 'info',
    env: 'LOG_LEVEL',
  }, 
  bot: {
    prefix: {
      doc: 'prefix to identify messages for this bot',
      format: String,
      default: '!s',
      env: 'BOT_PREFIX',
    },
    token: {
      doc: 'Discord Bot token',
      format: String,
      default: '',
      sensitive: true,
      env: 'BOT_TOKEN',
    }
  },
  storage: {
    settingsDir: {
      doc: 'Directory to store user settings data in',
      format: String,
      default: `${os.tmpdir()}/discord-shadorun-e6-dic-roll-bot/`,
      env: 'STORAGE_SETTINGS_DIR',
    }
  }
});

config.validate({allowed: 'strict'});

module.exports = config;
