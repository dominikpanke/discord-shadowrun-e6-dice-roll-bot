
module.exports = {
  DICE_ROLL: 'dice-roll',
  SHADOW_ROLL: 'shadow-roll',
  SET_USER_CONFIG: 'set-user-setting',
  GET_USER_CONFIG: 'get-user-settings',
};
