const logger = require('./log');
const discordClient = require('./discordClient');
const config = require('./config');
const { isBotMessage, parseBotMessage } = require('./helper');
const { handleCommand } = require('./commander');
const { transformBotMessage, transformError } = require('./transform');
const { loadUserSettings } = require('./usersettings');

 discordClient.once('ready', () => {
  // eslint-disable-next-line no-console
  logger.info('(: Bot ready :)');
});

// Create an event listener for messages
discordClient.on('message', async (message) => {
  if (isBotMessage(message)) {

    try {
      logger.debug('Received message for bot', { msg: message.content });
      const botMsg = parseBotMessage(message);

      const userSettings = await loadUserSettings(message.author.id);
      logger.debug('Loaded user settings', { userId: message.author.id, userSettings });

      logger.info('Valid message for bot received', { msg: message.content });
      const result = await handleCommand(botMsg, message.author.id, userSettings);
      logger.debug('Processed message successfully', { result: result });
      message.reply(transformBotMessage(result));
      
    } catch(error) {
      if(error.name === 'BotError') {
        logger.info('Message could not been processed', { msg: message.content, err: error.message } );
        message.reply(transformError(error));
      } else {
        logger.error('Unexpected error on message processing', { msg: message.content, err: error.message });
        // TODO: handle unknown errors in a user-friendly, descriptive way.
        message.reply('unknown-error');
      }
    }
  }
});

logger.info('Loaded configuration', { config: config.toString() });

logger.debug('Login bot at Discord');
discordClient.login(config.get('bot.token'));
