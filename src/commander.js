const logger = require('./log');

const { DICE_ROLL, SHADOW_ROLL, SET_USER_CONFIG, GET_USER_CONFIG } = require('./commands');
const { getDice } = require('./dices');
const roller = require('./roller');
const { BotError, codes } = require('./errors');
const { loadUserSettings, setUserSetting } = require('./usersettings');

const SHADOWRUN_SUCCESS_ROLL_THRESHOLD = 5;
const SHADOWRUN_GLITCH_ROLL_THRESHOLD = 1;
const SHADOWRUN_CRITICAL_ROLL_THRESHOLD = 1;

function rollShadowCmd(args, userConfig) {
  if (!args || args.length < 1) {
    throw new BotError(codes.INVALID_ARGS);
  }

  const amount = args[0];
  if(!Number.isInteger(amount) || amount < 1) {
    logger.info('invalid amount of dice past to shadow-roll', { args } );
    throw new BotError(codes.INVALID_ARGS);
  }

  const dice = getDice('d6');

  logger.info('Rolling dices', { dice: dice.kind, amount });
  const { rolls, total } = rollDiceMultipleTimes(dice, amount);
  logger.info('Rolled dices', { rolls, total })

  let successes = 0;
  let glitches = 0;
  let crits = 0;

  const glitchRollThreshold = userConfig ? userConfig.shadowrun.glitchRollThreshold :SHADOWRUN_GLITCH_ROLL_THRESHOLD;
  const critRollThreshold = userConfig ? userConfig.shadowrun.critRollThreshold : SHADOWRUN_CRITICAL_ROLL_THRESHOLD;
  for (let i = 0; i < rolls.length; i++) {
    const roll = rolls[i].result;
    if(roll >= SHADOWRUN_SUCCESS_ROLL_THRESHOLD) {
      successes++;
    } else if(roll <= glitchRollThreshold) {
      glitches++;

      if(roll <= critRollThreshold) {
        crits++;
      }
    }
  }

  
  const isGlitch = glitches > rolls.length / 2.0;
  const isCrit = crits > (rolls.length / 2.0) && successes == 0;

  return {
    rolls,
    total,
    successes, 
    glitches,
    glitch: isGlitch,
    crit: isCrit,
  }
}

function rollDiceCmd(args) {
  if (!args || args.length < 2) {
    throw new BotError(codes.INVALID_ARGS);
  }

  const dice = getDice(args[0]);
  if (!dice) {
    logger.info('invalid kind of dice past to dice-roll', { args } );
    throw new BotError(codes.INVALID_ARGS);
  }

  const amount = args[1];
  if(!Number.isInteger(amount) || amount < 1) {
    logger.info('invalid amount of dice past to dice-roll', { args } );
    throw new BotError(codes.INVALID_ARGS);
  }

  logger.info('Rolling dices', { dice: dice.kind, amount });
  const result = rollDiceMultipleTimes(dice, amount);
  logger.info('Rolled dices', { result })

  return result;
};

function rollDiceMultipleTimes(dice, amount) {
  let total = 0;
  const rolls = [];
  for(let r = 0; r < amount; r++) {
    const rollResult = roller.rollDice(dice);
    
    rolls.push({
      kind: dice.kind,
      result: rollResult,
    });

    total += rollResult;
  }

  return {
    total,
    rolls
  }
}

async function setUserSettingCmd(userId, args) {
  return {
    msg: await setUserSetting(userId, args[0], args[1]),
  };
}

async function getUserSettingsCmd(userId) {
  return {
    msg: JSON.stringify(await loadUserSettings(userId)),
  };
}

module.exports.handleCommand = async function (botMsg, userId, userConfig) {
  let result;

  switch(botMsg.command) {
    case DICE_ROLL: result = rollDiceCmd(botMsg.args); break;
    case SHADOW_ROLL: result = rollShadowCmd(botMsg.args, userConfig); break;
    case SET_USER_CONFIG: result = await setUserSettingCmd(userId, botMsg.args); break;
    case GET_USER_CONFIG: result = await getUserSettingsCmd(userId); break;
    default: throw new BotError(codes.UNKNONW_CMD);
  }

  return {
    command: botMsg.command,
    ...result,
  };
};