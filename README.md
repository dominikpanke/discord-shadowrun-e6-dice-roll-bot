# Discord Shadowrun e6 Dice Roll Bot

Simple Discord Bot for dice rolling in Shadowrun.

Disclaimer: This is a personal pet project to ease the live of my Shadowrun crew.

TODO:
- [ ] Add getting started 
- [ ] Add how to use
- [ ] Replace standard Javascript random numbers with real random numbers like from random.org
- [ ] Support initiative roles
