const winston = require('winston');
const config = require('./config');

module.exports = winston.createLogger({
  level: config.get('logLevel'),
  format: winston.format.json(),
  transports: [
    new winston.transports.Console({
      stderrLevels: ['error'],
      consoleWarnLevels: ['warn'],
    })
  ]
});
