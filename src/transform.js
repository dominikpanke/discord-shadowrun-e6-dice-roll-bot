const { MessageEmbed } = require('discord.js');
const { DICE_ROLL, SHADOW_ROLL } = require('./commands');

module.exports = {
  transformError: function (error) {
    return {
      content: error.message
    };
  },
  transformBotMessage: function (botMsg) {
    const embed = new MessageEmbed()
      .setColor('#99406c');

    switch(botMsg.command) {
      case DICE_ROLL: return module.exports._transformDiceRoll(botMsg, embed);
      case SHADOW_ROLL: return module.exports._transformShadowRoll(botMsg, embed);
      default: return botMsg.msg;
    };
  },
  _transformDiceRoll(botMsg, embed) {
    return embed
      .setTitle(botMsg.total)
      .setDescription(module.exports._descripeEquation(botMsg.rolls, botMsg.total));
  },
  _descripeEquation: function(rolls, total) {
    let description = '';
    for(let i = 0; i < rolls.length; i++) {
      description += rolls[i].result.toString();
      if(i + 1 < rolls.length) {
        description += ' + ';
      }
    }

    return description + ` = ${total}`;
  },
  _transformShadowRoll: function(botMsg, embed) {
    return embed
      .setTitle(module.exports._formatShadowRollTitle(botMsg))
      .setDescription(module.exports._descripeShadowEquation(botMsg.rolls, botMsg.total));
  },
  _descripeShadowEquation: function(rolls, total) {
    const equation = module.exports._descripeEquation(rolls, total);
    let euquationParts = equation.split('=');
    
    euquationParts[0] = euquationParts[0].replace(/6/g, '**6**');
    euquationParts[0] = euquationParts[0].replace(/5/g, '**5**');
    euquationParts[0] = euquationParts[0].replace(/1/g, '*1*');

    return `${euquationParts[0]}=${euquationParts[1]}`;
  },
  _formatShadowRollTitle: function(botMsg) {
    // TODO: add translation / language support to shadow-roll title
    if (botMsg.crit) {
      return 'Kritischer Patzer!'
    } else {
      const glitchHint = botMsg.glitch ? 'Patzer! - ' : '';
      const successesLabel = botMsg.successes == 1 ? 'Erfolg' : 'Erfolge';

      return `${glitchHint}${botMsg.successes} ${successesLabel}`;
    }
  }
};
