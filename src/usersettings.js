
const Keyv = require('keyv');
const KeyvFile = require('keyv-file').KeyvFile;
const config = require('./config');

const userSettingsKeyv = new Keyv({
  store: new KeyvFile({
    filename: `${config.get('storage.settingsDir')}user-settings.json`, // the file path to store the data
    expiredCheckDelay: 24 * 3600 * 1000, // ms, check and remove expired data in each ms
    writeDelay: 100, // ms, batch write to disk in a specific duration, enhance write performance.
    encode: JSON.stringify, // serialize function
    decode: JSON.parse // deserialize function
  }),
});
userSettingsKeyv.on('error', err => winston.error('Keyv error', { err: err }));

const defaultSettings = {
  version: 2,
  shadowrun: {
    glitchRollThreshold: 1,
    critRollThreshold: 1,
  },
};
const settingsUpgrades = {
  "1-2": function(settings) {
    return {
      version: 2,
      shadowrun: {
        glitchRollThreshold: settings.shadowrun.glitchThreshold,
        critRollThreshold: 1,
      }
    };

    return settings;
  },
};



module.exports = {
  loadUserSettings: async function (userId) {
    const rawUserSettings = await userSettingsKeyv.get(userId);

    if (!rawUserSettings) {
      return defaultSettings;
    }

    let userSettings = JSON.parse(rawUserSettings)

    userSettings = module.exports.upgradeUserSettings(userId, userSettings);

    return userSettings;
  },
  setUserSetting: async function (userId, key, value) {
    const userSettings = await module.exports.loadUserSettings(userId);

    switch(key) {
      case 'shadow-glitch-roll-threshold': userSettings.shadowrun.glitchRollThreshold = value; break;
      case 'shadow-crit-roll-threshold': userSettings.shadowrun.critRollThreshold = value; break;
      default: 'unknown setting';
    }

    userSettingsKeyv.set(userId, JSON.stringify(userSettings));
    return 'ok';
  },
  upgradeUserSettings(userId, userSettings) {
    if (!userSettings.version || userSettings.version < defaultSettings.version) {
      if (!userSettings.version) {
        userSettings = settingsUpgrades["1-2"](userSettings);
        userSettingsKeyv.set(userId, JSON.stringify(userSettings));
      }
    }
  
    return userSettings;
  }
}
